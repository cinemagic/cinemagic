import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { LogoutComponent } from './logout/logout.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieBookingComponent } from './movie-booking/movie-booking.component';
import { MovieSeatBookingComponent } from './movie-seat-booking/movie-seat-booking.component';
import { ForgotPasswordsComponent } from './forgot-passwords/forgot-passwords.component';



const routes: Routes = [
  
  {path:'',            component:HomepageComponent},
  {path:'homepage',    component:HomepageComponent},
  // {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},

  {path:'about',    component:AboutUsComponent},
  {path: 'movies', component:MoviesComponent},
  {path: 'movie-booking', component:MovieBookingComponent},
  {path: 'movie-seat-booking', component:MovieSeatBookingComponent},
  {path: 'logout', component:LogoutComponent},
  {path:'forgot-passwords',    component:ForgotPasswordsComponent},
  



];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }