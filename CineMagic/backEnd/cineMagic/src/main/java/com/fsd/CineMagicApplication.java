package com.fsd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.controllers.EmailSenderService;

@EntityScan(basePackages="com.model")
@EnableJpaRepositories(basePackages="com.dao")
@SpringBootApplication(scanBasePackages="com")
public class CineMagicApplication {

	@Autowired
	private EmailSenderService senderService;

	public static void main(String[] args) {
		SpringApplication.run(CineMagicApplication.class, args);
	}
	

	@EventListener(ApplicationReadyEvent.class)
	public void sendMail(){
		senderService.sendEmail("vaishnavikanchemeedi@gmail.com", "CineMagic", "Welcome to CineMagic application");
	}


}
