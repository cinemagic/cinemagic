import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
 
  isUserLogged: boolean;
  loginStatus: Subject<any>;
  private userDataKey = 'userData';

  constructor(private http: HttpClient) {
    this.isUserLogged = false;
    this.loginStatus = new Subject();
  }

  setUserData(user: any): void {
    localStorage.setItem(this.userDataKey, JSON.stringify(user));
  }

  getUserData(): any {

    const storedUserData = localStorage.getItem(this.userDataKey);
    return storedUserData ? JSON.parse(storedUserData) : null;

  }

  getUser(loginForm: any): any {
    return this.http
      .get(
        'http://localhost:8085/userLogin/' +
          loginForm.email +
          '/' +
          loginForm.password
      )
      .toPromise();
  }

  getUserId(): string | null {
    const userData = this.getUserData();
    return userData ? userData.userId : null;
  }

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  setUserLogout() {
    console.log('user logged out');
    localStorage.removeItem(this.userDataKey);
    this.isUserLogged = false;
  }

  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  registerUser(user: any) {
    return this.http.post('http://localhost:8085/addUser/', user);
  }

  checkUsername(username: any) {
    return this.http.get('http://localhost:8085/getUserByName/'+ username);
  }

  checkemailId(email: any) {
    return this.http.get('http://localhost:8085/getUserByEmail/'+ email);
  }

  getProfileInfo(userId: any) {
    return this.http.get('http://localhost:8085/getUserById/' + userId);
  }

  updateDetailsUser(profileInfo: any) {
    return this.http.put('http://localhost:8085/updateUser/',profileInfo);
  }

  updatePassWithMail(email:any , password:any){
    return this.http.post(`http://localhost:8080/updatePasswordEmail/${email}/${password}`,null);
  }

  updatePassWithPhone(phone:any , password:any){

    return this.http.post(`http://localhost:8080/updatePassword/${phone}/${password}`, null);
  }

  sendOTPtoPhone(forgotPass:any){
    return this.http.get('http://localhost:8080/sendsms/+91'+forgotPass);
  }

  sendOTPtoMail(forgotPass:any){
    return this.http.get('http://localhost:8080/sendOTP/'+forgotPass);
  }
}
