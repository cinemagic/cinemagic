import { Component} from '@angular/core';

@Component({
  selector: 'app-movie-seat-booking',
  templateUrl: './movie-seat-booking.component.html',
  styleUrl: './movie-seat-booking.component.css'
})
export class MovieSeatBookingComponent {

  movieList: any[]=[
    {
      movieName: 'IF',
      ticketRate:'250',
      description:'IF is about the girl who discovers that she can see everyones imaginary friends - and what she does with that superpower -as she embarks on magical adventure to reconnect forgotten IFs with their kids.',
      shows:[ '09.00 AM - 12.00 PM', '12.30 PM - 01.30 PM', '01.30 PM - 03.30 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'Krishnamma',
      ticketRate:'300',
      description:'The film revolves around three orphans who grew up as best friends on the banks of the Krishna River in Vijayawada and how they became each other`s family',
      shows:['10.00 AM - 12.00 PM', '03.00 PM - 06.00 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'Srikanth',
      ticketRate:'200',
      description:'In a world with odds stacked against him in every step of the way, Srikanth defies norms, blazing a trail from rural India to becoming the first visually impaired student at MIT',
      shows:['04.00 PM - 07.00 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'Prathinidhi 2',
      ticketRate:'240',
      description:'Chai serves as the CEO of NNC and has an unwavering commitment to expose political corruption. When a shocking incident implicating Chief Minister Prajapati occurs, Chai becomes the prime suspect.',
      shows:['11.00 AM - 02.00 PM', '02.00 PM - 05.00 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'Aa Okkati Adakku',
      ticketRate:'200',
      description:'A group of family and friends in a residential association experience a major shift upon the arrival of a peculiar new tenant',
      shows:[ '10.00 AM - 12.00 PM', '11.00 PM - 02.00 PM', '02.00 PM - 05.00 PM', '05.00 PM - 07.00 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'Baak',
      ticketRate:'250',
      description:'Baak is a Telugu movie starring Sundar C., Tamannaah Bhatia, Raashi Khanna and Yogi Babu in prominent roles. It is written and directed by Sundar C',
      shows:[ '09.00 AM - 12.00 PM', '12.30 PM - 01.30 PM', '01.30 PM - 03.30 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'The Boy And Then Heron',
      ticketRate:'200',
      description:'The Boy And The Heron follows the magical journey of a teenager Mahito in a completely new world. Experiencing the pain of losing his mother and having complicated relationships with his family and classmates, Mahito gradually isolates himself; until he meets a strange talking heron',
      shows:['10.00 AM - 12.00 PM', '03.00 PM - 06.00 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
    {
      movieName: 'The Fall Guy',
      ticketRate:'200',
      description:'An all-new adventure that pits the almighty Kong and the fearsome Godzilla against a colossal undiscovered threat hidden within our world, challenging their very existence-and our own',
      shows:['04.00 PM - 07.00 PM'],
      seatRows:[
        {row:'A' ,noOfSeats:9},
        {row:'B' ,noOfSeats:9},
        {row:'C' ,noOfSeats:9},
        {row:'D' ,noOfSeats:9},
        {row:'E' ,noOfSeats:9},
        {row:'F' ,noOfSeats:9},
        {row:'G' ,noOfSeats:9},
        {row:'H' ,noOfSeats:9},
      ]
    },
  ];
  selectedMovie: string='';
  selectedMovieObj:any;
  bookedSeatNoList:any[]=[];


  onMovieChange(){
    debugger;
    const movieData=this.movieList.find(m=>m.movieName==this.selectedMovie);
    if(movieData!=undefined){
      this.selectedMovieObj=movieData;
    }
  }
  
  getSeatNoArray( totalSeats: number){
    
    let seatArray=[];
    for(let i=1;i <= totalSeats;i++){
      seatArray.push(i)
    }
    return seatArray;
  }
  bookSeat(rowName: any, seatNo: any){
    const isDataExist=this.bookedSeatNoList.find(m=>m.rowName== rowName && m.seatNo==seatNo);
if(isDataExist == undefined){
  const seatObj={
    rowName:rowName,
    seatNo:seatNo
};
this.bookedSeatNoList.push(seatObj)
}else{
  const rowIndexToDelete=this.bookedSeatNoList.findIndex(m=>m.rowName== rowName && m.seatNo==seatNo);
  this.bookedSeatNoList.splice(rowIndexToDelete, 1)
}
    
  }
  checkIfSeatIsBooked(row: any, seatNo: any){
    const isDataExist=this.bookedSeatNoList.find(m=>m.rowName== row && m.seatNo==seatNo);
    if(isDataExist == undefined){
    return false;
}else{
  return true;
}
  }
}
