import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit {

  user: any;
  countries: any;

  constructor(private service: UserService) {

    this.user = {
      userName: '',
      gender: '',
      country: '',
      mobileNum: '',
      emailId: '',
      password: ''

    }
  }

  confirmPassword: string = '';

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
  }


  submit() {
    console.log(this.user);
    this.service.registerUser(this.user).subscribe((data: any) => {
      console.log(data);
    });
  }

}

