import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordsComponent } from './forgot-passwords/forgot-passwords.component';
import { HomepageComponent } from './homepage/homepage.component';
// import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { LogoutComponent } from './logout/logout.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieBookingComponent } from './movie-booking/movie-booking.component';
import { MovieSeatBookingComponent } from './movie-seat-booking/movie-seat-booking.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    HomepageComponent,
    ForgotPasswordsComponent,
    // ResetpasswordComponent,
    LogoutComponent,
    MoviesComponent,
    MovieBookingComponent,
    ForgotPasswordsComponent,
    MovieSeatBookingComponent,
   
  
    // ForgotPasswordComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    
  ],
  providers: [],
  
  bootstrap: [AppComponent],
})
export class AppModule { }


